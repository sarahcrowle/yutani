#pragma once

#include <string>

#include <yu_state.h>
#include <imgui/imgui_sdl.h>

class yu_window {
public:
    yu_window(std::string name, int width, int height) : name(name), width(width), height(height) {
        buffer = new ImGuiSDL::Texture();
    }

    ~yu_window() {};
    
    void draw();
private:
    void update_tex();

    std::string name;
    ImGuiSDL::Texture *buffer;
    ImVec2 last_avail_size;

    int width;
    int height;

    bool has_never_properly_resized = true;
};