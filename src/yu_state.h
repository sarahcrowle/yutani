#pragma once

#include <SDL2/SDL.h>

struct yu_state {
    SDL_Window* window;
    SDL_Renderer* renderer;
};

extern yu_state yu_global_state;