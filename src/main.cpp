#include <iostream>
#include <map>

#include <SDL2/SDL.h>
#include <imgui/imgui.h>
#include <imgui/imgui_sdl.h>

#include <yu_window.h>
#include <yu_state.h>

std::map<std::string, yu_window> windows;
yu_state yu_global_state;

int main() {
    SDL_Init(SDL_INIT_EVERYTHING);

	yu_global_state.window = SDL_CreateWindow("Yutani", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_RESIZABLE);
	yu_global_state.renderer = SDL_CreateRenderer(yu_global_state.window, -1, SDL_RENDERER_SOFTWARE | SDL_RENDERER_PRESENTVSYNC);

	ImGui::CreateContext();
	ImGuiSDL::Initialize(yu_global_state.renderer, 800, 600);

	SDL_Texture* texture = SDL_CreateTexture(yu_global_state.renderer, SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_TARGET, 100, 100);
	SDL_SetRenderTarget(yu_global_state.renderer, texture);
	SDL_SetRenderDrawColor(yu_global_state.renderer, 255, 0, 255, 255);
	SDL_RenderClear(yu_global_state.renderer);
	SDL_SetRenderTarget(yu_global_state.renderer, nullptr);

    yu_window winnie_the_pooh("winnie the pooh", 512, 512);
    windows.emplace("winnie the pooh", winnie_the_pooh);

    ImGuiIO& io = ImGui::GetIO();
    io.IniFilename = NULL;

	bool running = true;
	while (running) {
		int wheel = 0;

		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
                running = false;
            } else if (e.type == SDL_WINDOWEVENT) {
				if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
					io.DisplaySize.x = static_cast<float>(e.window.data1);
					io.DisplaySize.y = static_cast<float>(e.window.data2);
				}
			} else if (e.type == SDL_MOUSEWHEEL) {
				wheel = e.wheel.y;
			}
		}

		int mouseX, mouseY;
		const int buttons = SDL_GetMouseState(&mouseX, &mouseY);

		// Setup low-level inputs (e.g. on Win32, GetKeyboardState(), or write to those fields from your Windows message loop handlers, etc.)
		
		io.DeltaTime = 1.0f / 60.0f;
		io.MousePos = ImVec2(static_cast<float>(mouseX), static_cast<float>(mouseY));
		io.MouseDown[0] = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
		io.MouseDown[1] = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);
		io.MouseWheel = static_cast<float>(wheel);

		ImGui::NewFrame();

		for (auto& [window_name, window] : windows) {
            window.draw();
        }

		SDL_SetRenderDrawColor(yu_global_state.renderer, 114, 144, 154, 255);
		SDL_RenderClear(yu_global_state.renderer);

		ImGui::Render();
		ImGuiSDL::Render(ImGui::GetDrawData());

		SDL_RenderPresent(yu_global_state.renderer);
	}

	ImGuiSDL::Deinitialize();

	SDL_DestroyRenderer(yu_global_state.renderer);
	SDL_DestroyWindow(yu_global_state.window);

	ImGui::DestroyContext();
    return 0;
}