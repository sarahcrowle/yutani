#include <cstdlib>
#include <ctime>

#include <imgui/imgui.h>
#include <imgui/imgui_sdl.h>

#include "yu_window.h"

// I guess just pray you don't call this often
void yu_window::update_tex() {
    ImVec2 avail_size = ImGui::GetContentRegionAvail();
    if (avail_size.x != this->last_avail_size.x || avail_size.y != this->last_avail_size.y) {
        delete buffer;
        buffer = new ImGuiSDL::Texture();

        buffer->Source = SDL_CreateTexture(yu_global_state.renderer, 
                        SDL_PIXELFORMAT_RGB332, 
                        SDL_TEXTUREACCESS_STREAMING, avail_size.x, avail_size.y);

        printf("resize from %dx%d to %dx%d\n", width, height, (int)avail_size.x, (int)avail_size.y);
        
        // we have to do this weird hack to ignore the first resize... which is always the wrong size
        if (avail_size.x != 0 && avail_size.y != 0)
            has_never_properly_resized = false;

        this->last_avail_size = avail_size;
    }
}

void yu_window::draw() {
    if (has_never_properly_resized)
        ImGui::SetNextWindowSize(ImVec2(width, height));
    
    ImGui::Begin(this->name.c_str()); 
    {
        update_tex();

        srand(time(0));

        unsigned char* pixels;
        int pitch;
        SDL_LockTexture(buffer->Source, NULL, (void**)&pixels, &pitch);

        for (int y = 0; y < last_avail_size.y; y++) {
            for (int x = 0; x < last_avail_size.x; x++) {
                pixels[x * y] = rand() % 255 + 1;
            }
        }

        SDL_UnlockTexture(buffer->Source);
        ImGui::Image(this->buffer->Source, last_avail_size);
    }
    ImGui::End();
}