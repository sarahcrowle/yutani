﻿#pragma once

#include <SDL2/SDL.h>
#include <assert.h>

struct ImDrawData;
struct SDL_Renderer;

namespace ImGuiSDL
{
	// Call this to initialize the SDL renderer device that is internally used by the renderer.
	void Initialize(SDL_Renderer* renderer, int windowWidth, int windowHeight);
	// Call this before destroying your SDL renderer or ImGui to ensure that proper cleanup is done. This doesn't do anything critically important though,
	// so if you're fine with small memory leaks at the end of your application, you can even omit this.
	void Deinitialize();

	// Call this every frame after ImGui::Render with ImGui::GetDrawData(). This will use the SDL_Renderer provided to the interfrace with Initialize
	// to draw the contents of the draw data to the screen.
	void Render(ImDrawData* drawData);

	struct Color
	{
		const float R, G, B, A;

		explicit Color(uint32_t color)
			: R(((color >> 0) & 0xff) / 255.0f), G(((color >> 8) & 0xff) / 255.0f), B(((color >> 16) & 0xff) / 255.0f), A(((color >> 24) & 0xff) / 255.0f) { }
		Color(float r, float g, float b, float a) : R(r), G(g), B(b), A(a) { }

		Color operator*(const Color& c) const { return Color(R * c.R, G * c.G, B * c.B, A * c.A); }
		Color operator*(float v) const { return Color(R * v, G * v, B * v, A * v); }
		Color operator+(const Color& c) const { return Color(R + c.R, G + c.G, B + c.B, A + c.A); }

		uint32_t ToInt() const
		{
			return	((static_cast<int>(R * 255) & 0xff) << 0)
				  | ((static_cast<int>(G * 255) & 0xff) << 8)
				  | ((static_cast<int>(B * 255) & 0xff) << 16)
				  | ((static_cast<int>(A * 255) & 0xff) << 24);
		}

		void UseAsDrawColor(SDL_Renderer* renderer) const
		{
			SDL_SetRenderDrawColor(renderer,
				static_cast<uint8_t>(R * 255),
				static_cast<uint8_t>(G * 255),
				static_cast<uint8_t>(B * 255),
				static_cast<uint8_t>(A * 255));
		}
	};

	struct Texture
	{
		SDL_Surface* Surface;
		SDL_Texture* Source;

		~Texture()
		{
			SDL_FreeSurface(Surface);
			SDL_DestroyTexture(Source);
		}

		Color Sample(float u, float v) const
		{
			const int x = static_cast<int>(std::round(u * (Surface->w - 1) + 0.5f));
			const int y = static_cast<int>(std::round(v * (Surface->h - 1) + 0.5f));

			const int location = y * Surface->w + x;
			assert(location < Surface->w * Surface->h);

			return Color(static_cast<uint32_t*>(Surface->pixels)[location]);
		}
	};
}
